.. -*- coding: utf-8 -*-

==============
GnuPG settings
==============

Here is my GnuPG settings.

.gnupg/gpg.conf
===============

I create ``.gnupg/gpg.conf`` file with the following content. ::

  default-key 0xE267B052364F028D

I specify my default key (since I had old RSA key too).


Let gpg-agent manage SSH key
============================

I create ``.gnupg/gpg-agent.conf`` file with the following content. ::

  enable-ssh-support

I edit the file /etc/X11/Xsession.options and comment out use-ssh-agent line,
so that Xsession doesn't invoke original ssh-agent.  We use gpg-agent as ssh-agent.

In the files /etc/xdg/autostart/gnome-keyring-ssh.desktop,
I have a line something like: ::

    OnlyShowIn=GNOME;Unity;MATE;

I edit this line to: ::

    OnlyShowIn=

So that no desktop environment enables gnome-keyring for ssh.

References
==========

* `Creating a new GPG key`_
* `Use OpenPGP Keys for OpenSSH, how to use gpg with ssh`_

.. _Creating a new GPG key: https://keyring.debian.org/creating-key.html
.. _Use OpenPGP Keys for OpenSSH, how to use gpg with ssh: https://www.programmierecke.net/howto/gpg-ssh.html
