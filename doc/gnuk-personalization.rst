===================================
Initial configuration of Gnuk Token
===================================

Invoke GnuPG with the option ``--card-edit`` with your new Gnuk Token.  ::

  $ gpg --card-edit

  Reader ...........: 234B:0000:FSIJ-2.2-F1420A7A:0
  Application ID ...: D276000124010200FFFEF1420A7A0000
  Application type .: OpenPGP
  Version ..........: 2.0
  Manufacturer .....: unmanaged S/N range
  Serial number ....: F1420A7A
  Name of cardholder: [not set]
  Language prefs ...: [not set]
  Salutation .......: 
  URL of public key : [not set]
  Login data .......: [not set]
  Signature PIN ....: forced
  Key attributes ...: ed25519 cv25519 ed25519
  Max. PIN lengths .: 127 127 127
  PIN retry counter : 3 3 3
  Signature counter : 0
  KDF setting ......: off
  UIF setting ......: Sign=off Decrypt=off Auth=off
  Signature key ....: [none]
  Encryption key....: [none]
  Authentication key: [none]
  General key info..: [none]
  
  gpg/card> 

It shows the status of the card (as same as the output of ``gpg --card-status``).

Then, GnuPG enters its own command interaction mode.  The prompt is ``gpg/card>``.


Gnuk specific things
====================

Gnuk doesn't allow setting passphrase before importing your private
keys.  Only **after** importing your private keys from your host PC,
it's possible to change the passphrase.  Thus, the oder of setup
procedure is: initial config, importing private keys, and setup
passphrase.

Gnuk supports "admin less mode" for your passphrase setting.  It's the
smartcard culture to have two passphrases (one for admin, another for
user).  Gnuk supports the use case where admin==user.

Note that Gnuk requires enabling KDF-DO at initial configuration.
Let's do that first.


KDF-DO initial configuration
============================

Firstly, enabling admin command, I setup KDF-DO.  (KDF-DO is a feature
of OpenPGP card to allow computation of key derivation function on
host side.  This is mandatory for Gnuk 2.2.  With KDF-DO, your private
keys on MCU's flash ROM can be encrypted securely with help on host
side.)  ::

  gpg/card> admin
  Admin commands are allowed
  
  gpg/card> kdf-setup single

Here, "kdf-setup" is the sub-command and "single" is to specify the
use case of single PIN (adminless).  If you prefer traditional two
PINs use case (of Admin PIN and User PIN), don't supply "single"
argument, but just type "kdf-setup" sub-command.

It asks factory setting Admin PIN (usually with pop-up window), which
is 12345678.


Personalize your Gnuk Token
===========================

Optionally, you can put your name, language (by lang sub-command),
salutation, url and login.  I only put login of mine.  ::

  gpg/card> login
  Login data (account name): gniibe


PIN input is needed (or not) everytime for signing
==================================================

Since I don't want to force PIN input everytime when signing, toggle
it to non-force-pin-for-signature. ::

  gpg/card> forcesig

Instead of forcing PIN input everytime when signing by Gnuk Token, I
have a configuration of private key file under gpg-agent with a line
of "Confirm: yes", so that I can acknowledge/authorize use of key
everytime on my desktop.


Quit from the interactive session
=================================

Then, I quit. ::
  
  gpg/card> quit

That's all in this step.
