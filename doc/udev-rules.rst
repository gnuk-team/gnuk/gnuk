===============================================
Device Configuration for Gnuk Token with libusb
===============================================

In order to use Gnuk Token with libusb, configuration of device is
needed for permissions.  Note that this is not needed for the case of
PC/SC Lite, as it has its own device configuration.


udev rules for Gnuk Token
=========================

In case of Debian, there is a file /lib/udev/rules.d/60-scdamon.rules,
when you install "scdaemon" package, and no need of another
configuration.

If it's not the case, please have a configuration file of udev for
Gnuk Token to give a desktop user the permission to use the device.
It's like in the file /etc/udev/rules.d/60-gnuk.rules.  ::

    ## Gnuk Token
    SUBSYSTEM=="usb", ATTR{product}=="Gnuk Token", ENV{ID_SMARTCARD_READER}="1", \
    ENV{ID_SMARTCARD_READER_DRIVER}="gnupg"
    SUBSYSTEM=="usb", ATTR{idVendor}=="234b", ATTR{idProduct}=="0000", \
    ENV{ID_SMARTCARD_READER}="1", ENV{ID_SMARTCARD_READER_DRIVER}="gnupg"
    SUBSYSTEM=="usb", ATTR{idVendor}=="1209", ATTR{idProduct}=="2440", \
    ENV{ID_SMARTCARD_READER}="1", ENV{ID_SMARTCARD_READER_DRIVER}="gnupg"

Usually, udev daemon automatically handles for the changes of configuration
files.  If not, please let the daemon reload rules::

  # udevadm control --reload-rules


udev rules for ST-Link/V2
=========================

For development of Gnuk, we use ST-Link/V2 as JTAG/SWD debugger.
We need to have a udev rule for ST-Link/V2.  It's like::

    ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="0483", \
    ATTR{idProduct}=="3748", TAG+="uaccess"

I had this in the file /etc/udev/rules.d/10-stlink.rules.

In case of Debian, /usr/lib/udev/rules.d/60-openocd.rules has the
configuration for ST-Link/V2.
