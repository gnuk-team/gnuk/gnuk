===============================
Generating keys on your host PC
===============================

In this section, we describe how to generate ECC keys.

Generating ECC keys on host PC
==============================

Here is an example session log to create newer ECC keys.

Firstly, we invoke gpg frontend with ``--quick-gen-key`` option, specifying a user ID. ::

    $ gpg --quick-gen-key "Kunisada Chuji <chuji@gniibe.org>" default

It askes passphrase for this **key on host PC** (usually, by pop-up
window).  Note that this is a passphrase for the key on host PC.  It
is different thing to the passphrase of Gnuk Token.  We enter two same
inputs two times (once for passphrase input, and another for
confirmation), <PASSWORD-KEY-ON-PC>.  

Then, GnuPG generates keys (one primary key and a subkey).  It emits
message about random bytes, but it soon finishes. ::

    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.
    gpg: revocation certificate stored as '/tmp/tmp.riPmBwzyyp/openpgp-revocs.d/7AE3BF51B0CBF2F67E98C82038AC1343DBBEA961.rev'
    public and secret key created and signed.

    pub   ed25519 2024-09-03 [SC] [expires: 2027-09-03]
          7AE3BF51B0CBF2F67E98C82038AC1343DBBEA961
    uid                      Kunisada Chuji <chuji@gniibe.org>
    sub   cv25519 2024-09-03 [E]

We have the primary key with ed25519, and encryption subkey with cv25519.

Next, we add an authentication subkey which can be used with OpenSSH.
As the use case is not that common, we need ``--expert`` option for GnuPG. ::
We invoke gpg frontend with ``--edit-key``, specifying the key ID. ::

    $ gpg --expert --edit-key 7AE3BF51B0CBF2F67E98C82038AC1343DBBEA961
    gpg (GnuPG) 2.4.5; Copyright (C) 2024 g10 Code GmbH
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    sec  ed25519/38AC1343DBBEA961
         created: 2024-09-03  expires: 2027-09-03  usage: SC  
         trust: ultimate      validity: ultimate
    ssb  cv25519/52CBE6AC87EAD054
         created: 2024-09-03  expires: never       usage: E   
    [ultimate] (1). Kunisada Chuji <chuji@gniibe.org>
        
Here, it displays that there are main key and a subkey.
It prompts sub-command with ``gpg>``.  We invoke ``addkey`` subcommand. ::

    gpg> addkey

It asks a kind of key, we input ``11`` to select ECC for authentication. ::

    Please select what kind of key you want:
       (3) DSA (sign only)
       (4) RSA (sign only)
       (5) Elgamal (encrypt only)
       (6) RSA (encrypt only)
       (7) DSA (set your own capabilities)
       (8) RSA (set your own capabilities)
      (10) ECC (sign only)
      (11) ECC (set your own capabilities)
      (12) ECC (encrypt only)
      (13) Existing key
    Your selection? 11

and then, we specify "Authenticate" capability. ::

    Possible actions for a ECC key: Sign Authenticate 
    Current allowed actions: Sign 
    
       (S) Toggle the sign capability
       (A) Toggle the authenticate capability
       (Q) Finished
    
    Your selection? a
    
    Possible actions for a ECC key: Sign Authenticate 
    Current allowed actions: Sign Authenticate 
    
       (S) Toggle the sign capability
       (A) Toggle the authenticate capability
       (Q) Finished
    
    Your selection? s
    
    Possible actions for a ECC key: Sign Authenticate 
    Current allowed actions: Authenticate 
    
       (S) Toggle the sign capability
       (A) Toggle the authenticate capability
       (Q) Finished
    
    Your selection? q

Then, it asks which curve.  We input ``1`` for "Curve25519". ::

    Please select which elliptic curve you want:
       (1) Curve 25519
       (2) Curve 448
       (3) NIST P-256
       (4) NIST P-384
       (5) NIST P-521
       (6) Brainpool P-256
       (7) Brainpool P-384
       (8) Brainpool P-512
       (9) secp256k1
    Your selection? 1

It asks expiration of the key. ::

    Please specify how long the key should be valid.
             0 = key does not expire
          <n>  = key expires in n days
          <n>w = key expires in n weeks
          <n>m = key expires in n months
          <n>y = key expires in n years
    Key is valid for? (0) 
    Key does not expire at all
    Is this correct? (y/N) y

And the confirmation. ::

    Really create? (y/N) y

It goes. ::

    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.

Then, it asks the passphrase, it is the passphrase of **key on host PC**.
It's the one we entered above as <PASSWORD-KEY-ON-PC>.  And the subkey
is added. ::
    
    sec  ed25519/38AC1343DBBEA961
         created: 2024-09-03  expires: 2027-09-03  usage: SC  
         trust: ultimate      validity: ultimate
    ssb  cv25519/52CBE6AC87EAD054
         created: 2024-09-03  expires: never       usage: E   
    ssb  ed25519/AC87B245DFC7AEAC
         created: 2024-09-03  expires: never       usage: A   
    [ultimate] (1). Kunisada Chuji <chuji@gniibe.org>

We type ``save`` to exit form gpg. ::

    gpg> save
    $ 

  
Backup the private key
======================

There are some ways to back up private key, such that backup .gnupg
directory entirely, or use of paperkey, etc.  Here, we describe backup
by ASCII file.  ASCII file is good, because it has less risk on
transfer.  Binary file has a risk to be modified on transfer.

Note that the key on host PC is protected by a passphrase (which
is <PASSWORD-KEY-ON-PC> in the example above).  Using the key
from the backup needs this passphrase.  It is common that
people will forget passphrase for backup.  Never forget it.
You have been warned.

To make ASCII backup for private key,
invokde GnuPG with ``--armor`` option and ``--export-secret-keys``
specifying the key identifier. ::

  $ gpg --armor --output <YOUR-SECRET>.asc --export-secret-keys <YOUR-KEY-ID>

From the backup,
we can recover privet key by invoking GnuPG with ``--import`` option. ::

  $ gpg --import <YOUR-SECRET>.asc

Note that both command invocations ask you the passphrase.
