Introduction
============


What's Gnuk?
------------

Gnuk is an implementation of USB cryptographic token for GNU Privacy
Guard.  Gnuk supports OpenPGP card protocol version 2, and it runs on
an MCU (currently STM32F103 and GD32F103 are supported) .

This document explains about Gnuk 2.2, which comes with ECC support.


Cryptographic token and feature of Gnuk
---------------------------------------

Cryptographic token is a store of private keys and it computes cryptographic
functions on the device.

The idea is to separate important secrets to an independent device, 
from where nobody can extract them.


Development Environment
-----------------------

See :doc:`development` for development environment for Gnuk.
Gnuk is developed on the environment where there are only Free Software.


Target boards for running Gnuk
------------------------------

Hardware requirement for Gnuk is the micro controller STM32F103 or GD32F103.
In version 2.2, Gnuk supports following boards.

* FST-01 (Flying Stone Tiny ZERO-ONE), FST-01G, and FST-01SZ

* Olimex STM32-H103

* ST Nucleo F103


Host prerequisites for using Gnuk Token
---------------------------------------

* GNU Privacy Guard (GnuPG)

* libusb

* [Optional] SSH: openssh

* [experimental] Web: scute, firefox


Usages
------

* Sign with GnuPG
* Decrypt with GnuPG
* Use with OpenSSH through gpg-agent (as ssh-agent)
* [experimental] Use with Firefox through Scute for X.509 client certificate authentication
